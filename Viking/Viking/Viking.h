#pragma once
#include <iostream>
#include <vector>

const int RANGE_WIDTH = 100;
const int RANGE_HEIGHT = 100;
const int RANGE_ATTACK = 5;
const int RANGE_LOOKING = 15;
const int VIKING_POWER = 25;

class Viking
{
private:
	uint32_t m_groupId;
	int m_power;
	std::pair<uint32_t, uint32_t> m_location;

	void Viking::numOfObjectInRange(std::vector<Viking> players, int &numOfFriends, int &numOfEnemys);
public:
	Viking(uint32_t groupId, int m_power, std::pair<uint32_t, uint32_t> location);
	Viking(const Viking& viking);

	bool ObjectInRangeRange(Viking& player);
	bool FightOrFlight(std::vector<Viking> Players);
	bool moveVikinUp();
	bool moveVikinDown();
	bool moveVikinLeft();
	bool moveVikinRight();
};