#include "Viking.h"
#include <random>

int main()
{
	std::vector<Viking> vec;
	//create players
	for(size_t i = 0; i < 100; i++)
	{
		uint32_t groupId = rand() % 2;
		std::pair<uint32_t, uint32_t> location1(0, 0);
		std::pair<uint32_t, uint32_t> location2(RANGE_WIDTH, RANGE_HEIGHT);
		if(groupId == 0)
		{
			vec.push_back(Viking(groupId, VIKING_POWER, location1));
		}
		else if(groupId == 1)
		{
			vec.push_back(Viking(groupId, VIKING_POWER, location2));
		}
	}
	//
	return 0;
}