#include "Viking.h"

void Viking::numOfObjectInRange(std::vector<Viking> players, int &numOfFriends, int &numOfEnemys)
{
	numOfFriends = 0;
	numOfEnemys = 0;
	for each (const Viking player in players)
	{
	
			double range_to_object = std::sqrt(((player.m_location.first - m_location.first) * (player.m_location.first - m_location.first)) +
				((player.m_location.second - m_location.second) * (player.m_location.second - m_location.second)));
			if(RANGE_LOOKING >= range_to_object)
			{
				if(player.m_groupId == m_groupId)
				{
					numOfFriends++;
				}
				else
				{
					numOfEnemys++;
				}
			}
	}
}

Viking::Viking(uint32_t groupId, int power, std::pair<uint32_t, uint32_t> location) :
	m_groupId(groupId),
	m_power(power),
	m_location(location){}

Viking::Viking(const Viking& viking) :
	m_groupId(viking.m_groupId),
	m_power(viking.m_power),
	m_location(viking.m_location){}

bool Viking::FightOrFlight(std::vector<Viking> players)
{
	int friends, enemy;
	numOfObjectInRange(players, friends, enemy);
	return (friends - enemy) > 10;
}

bool Viking::ObjectInRangeRange(Viking& player)
{
	double range_to_object = std::sqrt(((player.m_location.first - m_location.first) * (player.m_location.first - m_location.first)) +
		((player.m_location.second - m_location.second) * (player.m_location.second - m_location.second)));
	return (RANGE_ATTACK >= range_to_object);
}

bool Viking::moveVikinUp()
{
	if(m_location.second < RANGE_HEIGHT)
	{
		m_location.second++;
		return true;
	}
	return false;
}

bool Viking::moveVikinDown()
{
	if(m_location.second > 0)
	{
		m_location.second--;
		return true;
	}
	return false;
}

bool Viking::moveVikinLeft()
{
	if(m_location.first > 0)
	{
		m_location.first--;
		return true;
	}
	return false;
}

bool Viking::moveVikinRight()
{
	if(m_location.first < RANGE_WIDTH)
	{
		m_location.first++;
		return true;
	}
	return false;
}